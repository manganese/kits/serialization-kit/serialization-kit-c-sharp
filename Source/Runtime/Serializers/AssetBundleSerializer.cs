using System;
using System.Collections.Generic;

using UnityEngine;

namespace Manganese.SerializationKit {
  public static class AssetBundleSerializer {
    public static UnityAssetBundle SerializeAssetBundle(AssetBundle assetBundle, object metadata = null) {
      var assets = new List<UnityAsset>();

      foreach (var path in assetBundle.GetAllAssetNames()) {
        assets.Add(AssetSerializer.SerializeAsset(path, assetBundle.LoadAsset(path)));
      }

      return new UnityAssetBundle() {
        assets = assets,
        metadata = metadata
      };
    }
  }
}