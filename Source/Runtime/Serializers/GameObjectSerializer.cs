using System;
using System.Collections.Generic;

using UnityEngine;

namespace Manganese.SerializationKit {
  public static class GameObjectSerializer {
    public static UnityGameObject SerializeGameObject(GameObject gameObject) {
      // Serialize components
      var components = new List<UnityComponent>();

      foreach (var component in gameObject.GetComponents<Component>()) {
        components.Add(ComponentSerializer.SerializeComponent(component));
      }

      // Serialize children
      var children = new List<UnityGameObject>();

      foreach (var child in gameObject.transform) {
        children.Add(SerializeGameObject((child as Transform).gameObject));
      }

      return new UnityGameObject() {
        name = gameObject.name,
        children = children,
        components = components
      };
    }
  }
}