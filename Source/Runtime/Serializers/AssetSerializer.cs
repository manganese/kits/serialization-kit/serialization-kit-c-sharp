using System;
using System.Collections.Generic;

using UnityEngine;

namespace Manganese.SerializationKit {
  public static class AssetSerializer {
    public static UnityAsset SerializeAsset(string path, UnityEngine.Object rawAsset) {
      var id = rawAsset.GetInstanceID();
      var name = rawAsset.name;

      if (rawAsset is TextAsset) {
        return new UnityTextAsset() {
          path = path,
          id = id,
          name = name
        };
      } else if (rawAsset is GameObject) {
        return new UnityPrefabAsset() {
          path = path,
          id = id,
          name = name,
          rootGameObject = GameObjectSerializer.SerializeGameObject(rawAsset as UnityEngine.GameObject)
        };
      } else if (rawAsset is Material) {
        return new UnityMaterialAsset() {
          path = path,
          id = id,
          name = name
        };
      } else {
        return new UnityAsset() {
          path = path,
          id = id,
          name = name,
          type = UnityAssetType.UNKNOWN
        };
      }
    }
  }
}