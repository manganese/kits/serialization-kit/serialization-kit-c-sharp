using System;
using System.Collections.Generic;

using UnityEngine;

namespace Manganese.SerializationKit {
  public static class ComponentSerializer {
    public static UnityComponent SerializeComponent(Component rawComponent) {
      var id = rawComponent.GetInstanceID();

      if (rawComponent is Transform) {
        return new UnityTransformComponent() {
          id = id
        };
      } else if (rawComponent is BoxCollider) {
        return new UnityBoxColliderComponent() {
          id = id
        };
      } else {
        return new UnityComponent() {
          id = id,
          type = UnityComponentType.UNKNOWN
        };
      }
    }
  }
}