using System;


namespace Manganese.SerializationKit {
  public enum UnityAssetType {
    UNKNOWN = 0,
    TEXT_ASSET = 1,
    PREFAB = 2,
    MATERIAL = 3
  }
}