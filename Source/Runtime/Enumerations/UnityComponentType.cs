using System;

namespace Manganese.SerializationKit {
  public enum UnityComponentType {
    UNKNOWN = 0,
    TRANSFORM = 1,
    BOX_COLLIDER = 2
  }
}