namespace Manganese.SerializationKit {
  public class UnityTransformComponent : UnityComponent {
    public override UnityComponentType type {
      get {
        return UnityComponentType.TRANSFORM;
      }
    }
  }
}