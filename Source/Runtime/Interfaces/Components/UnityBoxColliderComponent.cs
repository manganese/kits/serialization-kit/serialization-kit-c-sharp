namespace Manganese.SerializationKit {
  public class UnityBoxColliderComponent : UnityComponent {
    public override UnityComponentType type {
      get {
        return UnityComponentType.BOX_COLLIDER;
      }
    }
  }
}