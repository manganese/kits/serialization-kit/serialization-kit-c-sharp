using System.Collections.Generic;

using UnityEngine;

namespace Manganese.SerializationKit {
  public class UnityComponent {
    public int id;

    public virtual UnityComponentType type { get; set; }
  }
}