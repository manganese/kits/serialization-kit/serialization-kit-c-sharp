using UnityEngine;

namespace Manganese.SerializationKit {
  public class UnityPrefabAsset : UnityAsset {
    public override UnityAssetType type {
      get {
        return UnityAssetType.PREFAB;
      }
    }

    public UnityGameObject rootGameObject;
  }
}