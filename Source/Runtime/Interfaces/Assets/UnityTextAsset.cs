namespace Manganese.SerializationKit {
  public class UnityTextAsset : UnityAsset {
    public override UnityAssetType type {
      get {
        return UnityAssetType.TEXT_ASSET;
      }
    }
  }
}