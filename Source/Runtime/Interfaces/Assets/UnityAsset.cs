using System;

using Newtonsoft.Json;

namespace Manganese.SerializationKit {
  public class UnityAsset {
    public string path;

    public int id;

    public string name;

    public virtual UnityAssetType type { get; set; }

    // JSON serialization
    public string ToJSON() {
      return JsonConvert.SerializeObject(this);
    }
  }
}