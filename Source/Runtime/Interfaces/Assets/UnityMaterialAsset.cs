namespace Manganese.SerializationKit {
  public class UnityMaterialAsset : UnityAsset {
    public override UnityAssetType type {
      get {
        return UnityAssetType.MATERIAL;
      }
    }
  }
}