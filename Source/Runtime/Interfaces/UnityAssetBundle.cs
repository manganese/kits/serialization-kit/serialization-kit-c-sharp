using System;
using System.Collections.Generic;

using Newtonsoft.Json;


namespace Manganese.SerializationKit {
  public class UnityAssetBundle {
    public List<UnityAsset> assets;
    public object metadata;

    // JSON serialization
    public string ToJSON() {
      return JsonConvert.SerializeObject(this);
    }
  }
}