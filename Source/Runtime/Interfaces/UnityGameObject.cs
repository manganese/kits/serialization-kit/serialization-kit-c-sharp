using System.Collections.Generic;

using UnityEngine;


namespace Manganese.SerializationKit {
  public class UnityGameObject {
    public string name;

    public List<UnityComponent> components;
    public List<UnityGameObject> children;
  }
}