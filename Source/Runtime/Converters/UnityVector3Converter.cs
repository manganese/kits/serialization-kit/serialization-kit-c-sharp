using System;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Manganese.SerializationKit.Converters {
  public class UnityVector3Converter : JsonConverter<Vector3> {
    public override void WriteJson(JsonWriter writer, Vector3 vector3, JsonSerializer serializer) {
      var vector3Object = new JObject();

      vector3Object.Add("x", JToken.FromObject(vector3.x, serializer));
      vector3Object.Add("y", JToken.FromObject(vector3.y, serializer));
      vector3Object.Add("z", JToken.FromObject(vector3.z, serializer));

      vector3Object.WriteTo(writer);
    }

    public override Vector3 ReadJson(JsonReader reader, Type objectType, Vector3 _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        // Vector3 is a value type and cannot be null

        return new Vector3();
      } else if (reader.TokenType == JsonToken.StartObject) {
        var vector3Object = JObject.Load(reader);

        if (
          vector3Object.ContainsKey("x") &&
          vector3Object.ContainsKey("y") &&
          vector3Object.ContainsKey("z")
        ) {
          var x = vector3Object["x"].Value<float>();
          var y = vector3Object["y"].Value<float>();
          var z = vector3Object["z"].Value<float>();

          return new Vector3(x, y, z);
        }
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}