using System;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Manganese.SerializationKit.Converters {
  public class UnityGameObjectConverter : JsonConverter<UnityGameObject> {
    public override void WriteJson(JsonWriter writer, UnityGameObject gameObject, JsonSerializer serializer) {
      var gameObjectObject = new JObject();

      // TODO

      gameObjectObject.WriteTo(writer);
    }

    public override UnityGameObject ReadJson(JsonReader reader, Type objectType, UnityGameObject _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      // TODO

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}