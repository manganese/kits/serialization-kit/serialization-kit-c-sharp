using System;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Manganese.SerializationKit.Converters {
  public class UnityVector2Converter : JsonConverter<Vector2> {
    public override void WriteJson(JsonWriter writer, Vector2 vector2, JsonSerializer serializer) {
      var vector2Object = new JObject();

      vector2Object.Add("x", JToken.FromObject(vector2.x, serializer));
      vector2Object.Add("y", JToken.FromObject(vector2.y, serializer));

      vector2Object.WriteTo(writer);
    }

    public override Vector2 ReadJson(JsonReader reader, Type objectType, Vector2 _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        // Vector2 is a value type and cannot be null

        return new Vector2();
      } else if (reader.TokenType == JsonToken.StartObject) {
        var vector2Object = JObject.Load(reader);

        if (
          vector2Object.ContainsKey("x") &&
          vector2Object.ContainsKey("y")
        ) {
          var x = vector2Object["x"].Value<float>();
          var y = vector2Object["y"].Value<float>();

          return new Vector2(x, y);
        }
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}