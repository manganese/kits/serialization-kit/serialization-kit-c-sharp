using System;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Manganese.SerializationKit.Converters {
  public class UnityQuaternionConverter : JsonConverter<Quaternion> {
    public override void WriteJson(JsonWriter writer, Quaternion quaternion, JsonSerializer serializer) {
      var quaternionObject = new JObject();

      quaternionObject.Add("x", JToken.FromObject(quaternion.x, serializer));
      quaternionObject.Add("y", JToken.FromObject(quaternion.y, serializer));
      quaternionObject.Add("z", JToken.FromObject(quaternion.z, serializer));
      quaternionObject.Add("w", JToken.FromObject(quaternion.w, serializer));

      quaternionObject.WriteTo(writer);
    }

    public override Quaternion ReadJson(JsonReader reader, Type objectType, Quaternion _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        // Quaternion is a value type and cannot be null

        return new Quaternion();
      } else if (reader.TokenType == JsonToken.StartObject) {
        var quaternionObject = JObject.Load(reader);

        if (
          quaternionObject.ContainsKey("x") &&
          quaternionObject.ContainsKey("y") &&
          quaternionObject.ContainsKey("z") &&
          quaternionObject.ContainsKey("w")
        ) {
          var x = quaternionObject["x"].Value<float>();
          var y = quaternionObject["y"].Value<float>();
          var z = quaternionObject["z"].Value<float>();
          var w = quaternionObject["w"].Value<float>();

          return new Quaternion(x, y, z, w);
        }
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}