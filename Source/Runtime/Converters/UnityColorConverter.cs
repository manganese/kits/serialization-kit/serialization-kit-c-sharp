using System;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Manganese.SerializationKit.Converters {
  public class UnityColorConverter : JsonConverter<Color> {
    public override void WriteJson(JsonWriter writer, Color color, JsonSerializer serializer) {
      var colorObject = new JObject();

      colorObject.Add("red", JToken.FromObject(color.r, serializer));
      colorObject.Add("green", JToken.FromObject(color.g, serializer));
      colorObject.Add("blue", JToken.FromObject(color.b, serializer));

      if (color.a != 1F) {
        colorObject.Add("alpha", JToken.FromObject(color.a, serializer));
      }

      colorObject.WriteTo(writer);
    }

    public override Color ReadJson(JsonReader reader, Type objectType, Color _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        // Color is a value type and cannot be null

        return new Color();
      } else if (reader.TokenType == JsonToken.StartObject) {
        var colorObject = JObject.Load(reader);

        if (
          colorObject.ContainsKey("red") &&
          colorObject.ContainsKey("green") &&
          colorObject.ContainsKey("blue")
        ) {
          var red = colorObject["red"].Value<float>();
          var green = colorObject["green"].Value<float>();
          var blue = colorObject["blue"].Value<float>();
          var alpha = 1F;

          if (colorObject.ContainsKey("alpha")) {
            alpha = colorObject["alpha"].Value<float>();
          }

          return new Color(red, green, blue, alpha);
        }
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}