

using System;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Manganese.SerializationKit.Converters {
  public class UnityComponentConverter : JsonConverter<UnityComponent> {
    public static JsonConverterCollection jsonConverterCollection {
      get {
        var jsonConverterCollection = new JsonConverterCollection();
        jsonConverterCollection.Add(new UnityComponentConverter());

        return jsonConverterCollection;
      }
    }

    public override void WriteJson(JsonWriter writer, UnityComponent component, JsonSerializer serializer) {
      var componentObject = new JObject();

      // TODO

      componentObject.WriteTo(writer);
    }

    public override UnityComponent ReadJson(JsonReader reader, Type objectType, UnityComponent _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      // TODO

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}